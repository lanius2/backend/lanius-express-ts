import { Entity, PrimaryGeneratedColumn, Column, Unique, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { IsNotEmpty } from 'class-validator';
import { User } from '../interfaces/users.interface';

@Entity()
@Unique(['email'])
export class UserEntity implements User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'email' })
  @IsNotEmpty()
  email: string;

  @Column({ name: 'password' })
  @IsNotEmpty()
  password: string;

  @Column({ name: 'full_name' })
  fullName: string;

  @Column({ name: 'phone' })
  phone: string;

  @Column({ name: 'address' })
  address: string;

  @Column({ name: 'create_at' })
  @CreateDateColumn()
  createdAt: Date;

  @Column({ name: 'update_at' })
  @UpdateDateColumn()
  updatedAt: Date;
}
