import { IsString } from 'class-validator';
import { CreateUserDto } from './users.dto';

export default class AccountDetailDto extends CreateUserDto {
  @IsString()
  public token?: string;
}
