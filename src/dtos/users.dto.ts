import { IsEmail, IsString } from 'class-validator';
import { User } from '../interfaces/users.interface';

export class CreateUserDto implements User {
  public id?: number;

  @IsEmail()
  public email: string;

  @IsString()
  public password?: string;

  @IsString()
  public fullName?: string;

  @IsString()
  public phone?: string;

  @IsString()
  public address?: string;
}
