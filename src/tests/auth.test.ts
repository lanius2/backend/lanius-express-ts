// import request from 'supertest';
// import App from '../app';
// import AuthRoute from '../routes/auth.route';
// import { CreateUserDto } from '../dtos/users.dto';

// afterAll(async () => {
//   await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
// });

//   describe('[POST] /login', () => {
//     it('response should have the Set-Cookie header with the Authorization token', async () => {
//       const userData: CreateUserDto = {
//         email: 'lanius@gmail.com',
//         password: 'lanius',
//       };
//       process.env.JWT_SECRET = 'jwt_secret';
//       const authRoute = new AuthRoute();
//       const app = new App([authRoute]);

//       return request(app.getServer())
//         .post('/login')
//         .send(userData)
//         .expect('Set-Cookie', /^Authorization=.+/);
//     });
//   });

//   describe('[POST] /logout', () => {
//     it('logout Set-Cookie Authorization=; Max-age=0', () => {
//       const authRoute = new AuthRoute();
//       const app = new App([authRoute]);

//       return request(app.getServer())
//         .post('/logout')
//         .expect('Set-Cookie', /^Authorization=\;/);
//     });
//   });
// });
