<div align="center">
 <h2>Lanius Backend</h2>
 <a href="https://standardjs.com">
 <img 
 src="https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat" 
 alt="Code Style"
 >
 </a>
 <a href="https://gitlab.com/jublia/public/directory_widget/-/tags/v1.0.0">
 <img 
 src="https://img.shields.io/static/v1.svg?label=version&message=1..0&style=flat&color=brightgreen" 
 alt="Version"
 >
 </a>
 <a href="https://conventionalcommits.org">
 <img 
 src="https://img.shields.io/badge/conventional%20commits-1.0.0-brightgreen.svg" 
 alt="Conventional Commits"
 >
 </a>
 <a href="https://jestjs.io">
 <img 
 src="https://jestjs.io/img/jest-badge.svg?style=flat" 
 alt="Jest"
 >
 </a>
</div>
<br />

## Requirement
 - [node.js](http://nodejs.org/)
 - [express](https://expressjs.com/)
## Quick Start
```bash
$ git clone https://gitlab.com/lanius2/backend/lanius-express-ts.git
# open folder lanius-express-ts
$ cd lanius-express-ts
# instal packages
$ npm install
# copy file env/env.dev to .env
$ cp env/env.dev .env
# build and serve with express
$ npm run dev
```
